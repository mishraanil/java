package employeedemo;

class Employee{
    String name;
    int age;
    float salary;
    
void show(){
        
        System.out.println("Name = " + this.name);
        System.out.println("Age = " + this.age);
        System.out.println("Salary = " + (this.salary + (this.salary * 0.10)));
        

}

}


public class EmployeeDemo {

    public static void main(String[] args) {
        
        Employee e = new Employee();
        Employee e1 = new Employee();
        
        
        e.name = "Anil";
        e.age = 25;
        e.salary = 500;
        
        e1.name = "Mishra";
        e1.age = 30;
        e1.salary = 1000;
        
        e.show();
        e1.show();
        
        
    }
    
}
