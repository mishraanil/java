package calculation;

import college.Student;
import java.math.MathContext;
import javax.swing.JOptionPane;

public class Calculation {

    public static void main(String[] args) {
        
//        Student s = new Student(0, "inputname");

        int x = Integer.parseInt(JOptionPane.showInputDialog("Enter x Value"));
        int y = Integer.parseInt(JOptionPane.showInputDialog("Enter y Value"));
        
        String operator = JOptionPane.showInputDialog("Please enter operator to perform action (+ - * / )");
        
        MyMaths m = new MyMaths(x, y);
        
        switch (operator) {

            case "+":
                System.out.println("Addition of " + x + " + " + y + " = " + m.add());
                break;
            case "-":
                System.out.println("Subtraction of " + x + " - " + y + " = " + m.sub());
                break;
            case "*":
                System.out.println("Multiplication of " + x + " * " + y + " = " + m.mul());
                break;
            case "/":
                System.out.println("Division of " + x + " / " + y + " = " + m.div());
                break;
            default:

                System.out.println("Invalid operator " + operator + " Please select any of this operator (+ - * /)");
                break;

        }  
        
        
        
        
        
        
        
        /*
        MyMaths m = new MyMaths();
        MyMaths m1 = new MyMaths(10, 100);
        int x;
        int y;

        x = Integer.parseInt(JOptionPane.showInputDialog("Enter x Value"));
        y = Integer.parseInt(JOptionPane.showInputDialog("Enter y Value"));

        String oprator = JOptionPane.showInputDialog("Please select the operator (+ - / *) to perform action");

        switch (oprator) {

            case "+":
                System.out.println("Addition of " + x + " and " + y + " = " + m.add(x, y));
                break;
            case "-":
                System.out.println("Subtraction of " + x + " and " + y + " = " + m.sub(x, y));
                break;
            case "/":
                System.out.println("Division of " + x + " and " + y + " = " + m.div(x, y));
                break;
            case "*":
                System.out.println("Multiplication of " + x + " and " + y + " = " + m.mul(x, y));
                break;
            default:
                System.out.println("Invalid Operator " + oprator + " Selected please select any of this (+ - * /");
                break;
        }
        /*        
        System.out.println("This addition is from Parameterless Constructor " + m.add());
        System.out.println("The below calculation is done using local variable");
        System.out.println("The addition of two variable is " + m.add(45, 65));
        System.out.println("The subtraction of two variavle is " + m.sub(10, 4));
        System.out.println("The division result is " + m.div(4, 2));
        System.out.println("The multiplication of two variable is " + m.mul(5, 5));
        System.out.println("This addition is from parameterized Constructor " + m1.add());

    }*/
    }
}
