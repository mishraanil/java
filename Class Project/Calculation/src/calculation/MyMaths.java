package calculation;

public class MyMaths {

    int x;
    int y;

    public MyMaths() {
        this.x = 1;
        this.y = 1;
    }

    public MyMaths(int x, int y) {

        this.x = x;
        this.y = y;

    }

    public int add() {
        return this.x + this.y;
    }

    public int add(int num1, int num2) {

        return num1 + num2;
    }

    public int sub(int x, int y) {
        return x - y;
    }

    public int mul(int x, int y) {
        return x * y;
    }

    public double div(int x, int y) {
        return x / y;
    }

    public int sub() {
        return this.x - this.y;
    }

    public int mul() {
        return this.x * this.y;
    }

    public int div() {
        return this.x / this.y;
    }

}
