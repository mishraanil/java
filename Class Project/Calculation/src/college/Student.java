/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package college;

/**
 *
 * @author pc
 */
public class Student {
    int roll_num;
    String name;
    String section;
    static String colleg_name = "IIT Bombay";
    
    public Student(){
        System.out.println("I am from default constructor Student");
    }
    
    public Student(int rollnum) {
        System.out.println("I am from parameterized constructor Student");
    }
    
    public Student(int rollnum, String inputname) {
        System.out.println("I am from parameterized constructor Student() with two parameter");
    }
}
